//url: wss://stream.binance.com


ws=new Ws("wss://testnet.binancefuture.com/stream?streams=btcusdt@depth");

ws.socket().addEventListener('error',(e)=>{
    console.error("Websocket Error: ",e);
});
ws.onopen(()=>{
    document.querySelector('#statut').textContent="Connected: waiting data";
    ws.socket().onmessage=(e)=>{
        document.querySelector('#statut').textContent="Obtaining data";
        // console.log("Reponse");
        let data=JSON.parse(e.data);
        document.querySelector("#streamValue").textContent=data.stream;     
        for(key in data.data)
        {
            let text="";
            if(data instanceof Array) text=data.data[key].join(', ');
            else text=data.data[key];
            // console.log(document.querySelector(`#${key}Value`),`#${key}Value`,text);
            try{
                document.querySelector(`#${key}Value`).innerHTML=text;
            }
            catch(e)
            {
                console.log(key);
            }
            
        }
        // document.querySelector('#main').appendChild(document.createTextNode(e.data));  
    };
    console.info("Ws are opened");

});