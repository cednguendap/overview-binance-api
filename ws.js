class Ws
{
	constructor(url)
	{
        this._url=url;		
        this._statut=-1;
		this._socket=new WebSocket(url);
		
	}
	async send(data)
	{
        if(this._statut==-1) throw Error("Websocket Error: You can use Websocket before is openned");
        this._socket.send(data);
	}
	onopen(func)
	{
		this._socket.addEventListener("open",()=>{
			console.log("Open Websocket");
			this._statut=0;
			func();
		});
	}
	is_open()
	{
		return this._statut!=-1;
	}
	socket()
	{
		return this._socket;
	}
	
}